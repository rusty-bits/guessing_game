extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Let's play a guessing game!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("Enter a number from 1 to 100");
        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("Read line failed");

        let guess: i32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("too small"),
            Ordering::Greater => println!("too big"),
            Ordering::Equal => {
                println!("you got it!");
                break;
            }
        }
    }
}
